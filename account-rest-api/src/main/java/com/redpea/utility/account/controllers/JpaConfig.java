package com.redpea.utility.account.controllers;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class JpaConfig {

	@Value("${spring.datasource.url}")
	private String url;

	@Value("${conatiner.datasource.host}")
	private String conatiner_datasource_host;

	@Value("${conatiner.datasource.port}")
	private String conatiner_datasource_port;

	@Value("${spring.datasource.username}")
	private String user;

	@Value("${spring.datasource.password}")
	private String password;

	@Bean(name = "mySqlDataSource")
	@Primary
	public DataSource mySqlDataSource() {
		@SuppressWarnings("rawtypes")
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();

		if (System.getenv(conatiner_datasource_host) != null) {
			url = "jdbc:mariadb://" + System.getenv(conatiner_datasource_host) + ":"
					+ System.getenv(conatiner_datasource_port) + "/mdm?serverTimezone=UTC";
		}

		System.out.println("URL is " + url);

		dataSourceBuilder.url(url);

		// dataSourceBuilder.url(System.getenv("ENV_URL"));

		dataSourceBuilder.username(user);
		dataSourceBuilder.password(password);

		return dataSourceBuilder.build();
	}

}
