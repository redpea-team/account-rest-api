package com.redpea.utility.account.controllers;

import java.util.List;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class Clense_Account_List {

	@Autowired
	DataSource dataSource;

	public List<String> getClensedAccountList(List<String> account_List) {

		StringBuilder tList = new StringBuilder();

		for (String acct : account_List) {
			if (tList.length() > 0)
				tList.append(",");
			tList.append("'").append(acct).append("'");
		}

		String sql = "SELECT brokerage_account FROM crf where brokerage_account in (" + tList + ")";

		// System.out.println(sql);

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		List<String> result = jdbcTemplate.queryForList(sql, String.class);

		System.out.println(result);

		return result;

	}

}
