package com.redpea.utility.account.controllers;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.redpea.account.utility.converter.Account_Formatter;
import com.redpea.account.utility.converter.Brokerage_Account;

/**
 *
 * Controller to return account conversion results
 */
@RestController
public class RequestsController {
	/**
	 *
	 * @param account number to be converted
	 * @return converted accounts list
	 * @throws JsonProcessingException
	 */

	@Autowired
	DataSource dataSource;
	
	@Autowired
	Clense_Account_List clense;
	

	@RequestMapping(value = "/account/convert/{account}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Brokerage_Account account_Convert(@PathVariable String account) throws JsonProcessingException {

		Account_Formatter engine = new Account_Formatter();

		List<String> account_List = engine.account_Convert(account);
		Brokerage_Account bacct = new Brokerage_Account();
		bacct.setAccount_in_digit(account);
		
		List<String> result = clense.getClensedAccountList(account_List);
		
		bacct.setBrokerage_account(result);

		return bacct;
	}

	@RequestMapping(value = "/account/convert/V2", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public Brokerage_Account account_Convert_V2(@RequestBody Brokerage_Account account) throws JsonProcessingException {

		Account_Formatter engine = new Account_Formatter();

		List<String> account_List = engine.account_Convert(account.getAccount_in_digit());
		Brokerage_Account bacct = new Brokerage_Account();
		bacct.setAccount_in_digit(account.getAccount_in_digit());

		bacct.setCalculated_account(account_List);

		// identify account that exist in CRF table
		
		List<String> result = clense.getClensedAccountList(account_List);
		
		// -----------------
		bacct.setBrokerage_account(result);
		return bacct;
	}

}
