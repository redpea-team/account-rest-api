package com.redpea.account.utility.converter;

import java.util.List;


public class Brokerage_Account {
	
	private String account_in_digit;
	private List<String> brokerage_account;
	private List<String> calculated_account;

	public List<String> getCalculated_account() {
		return calculated_account;
	}

	public void setCalculated_account(List<String> calculated_account) {
		this.calculated_account = calculated_account;
	}

	public Brokerage_Account() {
		// TODO Auto-generated constructor stub
	}

	public String getAccount_in_digit() {
		return account_in_digit;
	}

	public void setAccount_in_digit(String account_in_digit) {
		this.account_in_digit = account_in_digit;
	}

	public List<String> getBrokerage_account() {
		return brokerage_account;
	}

	public void setBrokerage_account(List<String> brokerage_account) {
		this.brokerage_account = brokerage_account;
	}

	

}
