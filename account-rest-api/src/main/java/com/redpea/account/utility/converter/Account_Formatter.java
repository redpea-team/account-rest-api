package com.redpea.account.utility.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Account_Formatter {

	Map<Integer, List<Character>> map = new HashMap<Integer, List<Character>>();

	// ML brokerage account format
	String default_account_format = "DAADDADD";

	final char ALPHA_CHARACTER = 'A';

	public Account_Formatter() {
		// TODO Auto-generated constructor stub
		map.put(0, Arrays.asList('0'));
		map.put(1, Arrays.asList('1'));
		map.put(2, Arrays.asList('A', 'B', 'C','2'));
		map.put(3, Arrays.asList('D', 'E', 'F','3'));
		map.put(4, Arrays.asList('G', 'H', 'I','4'));
		map.put(5, Arrays.asList('J', 'K', 'L','5'));
		map.put(6, Arrays.asList('M', 'N', 'O','6'));
		map.put(7, Arrays.asList('P', 'Q', 'R', 'S','7'));
		map.put(8, Arrays.asList('T', 'U', 'V','8'));
		map.put(9, Arrays.asList('W', 'X', 'Y', 'Z','9'));
	}

	// get the list to calculate the combination

	List<List<Character>> alphaCharacterList(String account_format, String account) {

		List<List<Character>> myList = new ArrayList<List<Character>>();

		int i = 0;
		for (char c : account.toCharArray()) {

			if (account_format.charAt(i) == ALPHA_CHARACTER) {
				List<Character> list = map.get(Character.getNumericValue(c));
				myList.add(list);
			}
			i++;

		}
		// combination list
		// System.out.println(myList.toString());

		return myList;

	}

	// calculate the combination recursively - based on # of elements
	// in each list
	void digitPermutations(List<List<Character>> lists, List<String> result, int depth, String current) {
		// add calculation to the result when reach to the last list
		if (depth == lists.size()) {
			result.add(current);
			return;
		}
		// Permutation from current list with next list
		for (int i = 0; i < lists.get(depth).size(); i++) {
			digitPermutations(lists, result, depth + 1, current + lists.get(depth).get(i));
		}
	}

	public List<String> account_Convert(String in_account) {

		return account_Convert(default_account_format, in_account);

	}

	public List<String> account_Convert(String account_format, String in_account) {

		List<String> account_list = new ArrayList<String>();

		// if account is not all number, then no conversion
		// and return original account

		if (is_Valid(account_format, in_account) == false) {
			return account_list;
		}

		if (is_Numeric(in_account) == false) {
			account_list.add(in_account);
			return account_list;
		}

		// build the possible combination of alpha characters list

		List<List<Character>> myList = alphaCharacterList(account_format, in_account);

		List<String> result = new ArrayList<String>();
		digitPermutations(myList, result, 0, "");
		for (String c : result) {
			String account = "";
			int x = 0; // index of account number position
			int j = 0; // index for alpha character position
			for (char act_format : account_format.toCharArray()) {
				if (act_format == 'A') {
					account += c.substring(j, j + 1);
					j++;
				} else {
					account += in_account.substring(x, x + 1);
				}
				x++;
			}
			account_list.add(account);

		}

		return account_list;
	}

	// are all account digits numeric?

	Boolean is_Numeric(String account) {

		Boolean ok = true;

		for (char c : account.toCharArray()) {

			if (Character.isDigit(c) == false) {
				ok = false;
				break;
			}

		}

		return ok;

	}

	// To ensure account is valid based on account format
	// definition

	Boolean is_Valid(String account_format, String account) {

		Boolean ok = true;

		if (account == null || account.isEmpty() || account.length() != account_format.length()) {
			ok = false;
		}

		return ok;

	}

}
